(function(doc, win) {
  const docEl = doc.documentElement;
  const resizeEvt =
    'orientationchange' in window ? 'orientationchange' : 'resize';
  const recalc = function() {
    const appWidth = window.innerWidth;
    // const { clientWidth: appWidth } = docEl;
    if (!appWidth) {
      return;
    }

    if (appWidth > 900 && appWidth < 1440) {
      docEl.style.fontSize = `${16 * (appWidth / 1440)}px`;
    } else if (appWidth <= 900) {
      docEl.style.fontSize = `${16 * (appWidth / 320)}px`;
    } else {
      docEl.style.fontSize = `16px`;
    }
  };

  if (!doc.addEventListener) return;
  win.addEventListener(resizeEvt, recalc, false);
  doc.addEventListener('DOMContentLoaded', recalc, false);
})(document, window);
