import React, { useEffect } from 'react';
import { MainHeader } from 'common/layouts/components';
import { RouteComponentProps } from 'react-router';

type IProps = RouteComponentProps & {
  children: React.ReactNode;
  [key: string]: any;
};

export const MainLayout: React.FC<IProps> = props => {
  const { children, menu = false, history, location, match } = props;
  const routerProps = { history, location, match };

  useEffect(() => {
    console.log('MainLayout MOUNTED!!');
  }, []);

  return (
    <div className="out">
      <MainHeader />
      {children}
    </div>
  );
};
