import { LAYOUT } from '../constants';
import { EmptyLayout } from '../EmptyLayout';
import { MainLayout } from '../MainLayout';

export const layoutMap = {
  [LAYOUT.EMPTY]: {
    component: EmptyLayout,
    props: null,
  },
  [LAYOUT.MAIN]: {
    component: MainLayout,
    props: null,
  },
};
