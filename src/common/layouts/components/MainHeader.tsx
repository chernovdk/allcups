import React from 'react';
import './MainHeader.scss';
import { HeaderLogo } from 'ui/logo';
import { BurgerButton } from './BurgerButton';

type Props = {};

export const MainHeader: React.FC<Props> = () => {
  return (
    <header className="header">
      <div className="containerFluid header__inner">
        <div className="header__left">
          <div className="header__logo">
            <a href="/" className="logo">
              <HeaderLogo />
            </a>
          </div>
          <div className="header__burger">
            <BurgerButton />
          </div>
        </div>
        <div className="header__right">
          <a href="/signup" className="btn btn--border">
            Авторизация
          </a>
        </div>
      </div>
    </header>
  );
};
