import React from 'react';
import './BurgerButton.scss';

type Props = {};

export const BurgerButton: React.FC<Props> = () => {
  return (
    <button className="btn--burger">
      <span className="bar"></span>
      <span className="bar"></span>
    </button>
  );
};
