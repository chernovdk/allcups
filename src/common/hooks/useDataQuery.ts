import { useState, useEffect } from 'react';
import { DataQueryFunction } from 'network';
import { DataQueryOptions } from './models';

export const useDataQuery = (
  query: DataQueryFunction | null,
  variables: object | null = null,
  options?: DataQueryOptions
) => {
  const [loading, setLoading] = useState(true);
  const [response, setResponse] = useState<any>(null);
  const [error, setError] = useState(null);

  const { skip = null } = options || {};

  const fetchData = () => {
    if (skip || !query) return;
    setLoading(true);
    query(variables)
      .then(res => {
        setResponse(res);
        setLoading(false);
      })
      .catch(err => {
        setError(err);
        setLoading(false);
      });
  };

  const refetch = () => {
    if (!loading) fetchData();
  };

  useEffect(() => {
    fetchData();
  }, [query, skip]); // eslint-disable-line

  return {
    response,
    data: (response && response.data) || null,
    loading,
    error,
    refetch,
  };
};
