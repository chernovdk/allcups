import React from 'react';
import './CategoryBanner.scss';
import { SvgIcon } from 'ui/components';

type Props = {};

export const CategoryBanner: React.FC<Props> = () => {
  return (
    <div className="section section--category">
      <div className="container section__inner">
        <div
          className="card--category-banner"
          style={{ backgroundColor: '#73CA86' }}
        >
          <div className={`card__decor icon-mashin_decor`}>
            <SvgIcon icon="mashin_decor" />
          </div>

          <div className="card__title">Машинное обучение</div>
          <div className="card__text">
            Уникальные наборы данных для самых смелых исследователей
          </div>
        </div>
      </div>
    </div>
  );
};
