import React from 'react';
import './Banner.scss';
import { SvgIcon } from 'ui/components';

type Props = {};

export const Banner: React.FC<Props> = () => {
  return (
    <div className="section section--banner">
      <div className="container section__inner">
        <div className="banner" style={{ background: '#365FF6' }}>
          <div className="banner__left">
            <SvgIcon icon={'banner_picture'} />

            {/* {{ mixins.icon('banner_picture') }} */}
          </div>
          <div className="banner__right">
            <div className="banner__title">All cups</div>
            <div className="banner__subtitle">
              Площадка, где специалисты и эксперты отрасли информационных
              технологий развиваются и обмениваются опытом, решая нетривиальные
              и интересные задачи в крупнейших чемпионатах.
            </div>

            <div className="banner__bottom">
              <div className="banner__slogan">
                <span>Нас уже более 150 000 человек.</span>
              </div>

              <div className="banner__link">
                <a href="/" className="btn btn--move btn--white">
                  <span>Присоединяйся</span>
                  <SvgIcon icon={'arrow_right'} />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
