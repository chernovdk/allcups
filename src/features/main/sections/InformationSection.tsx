import React from 'react';
import './InformationSection.scss';
import { InformationItem } from '../components/InformationItem';
import { InformationSlider } from '../sliders';

type Props = {};

export const InformationSection: React.FC<Props> = () => {
  const items = [1, 2, 3].map(item => <InformationItem key={item} />);

  return (
    <div className="section section--information">
      <div className="container section__inner">
        <div className="sectionHead">
          <div className="section__title">Дополнительная информация</div>
        </div>

        <ul className="informationList">{items}</ul>
        <InformationSlider>{items}</InformationSlider>
      </div>
    </div>
  );
};
