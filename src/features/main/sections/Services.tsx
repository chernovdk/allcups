import React from 'react';
import './Banner.scss';
import { ServiceCard } from '../components';
import './Services.scss';
import { useSelector } from 'react-redux';
import { categoriesSelector } from 'store';

type Props = {};

export const Services: React.FC<Props> = () => {
  const categories = useSelector(categoriesSelector);
  if (!Array.isArray(categories)) return null;

  return (
    <div className="section section--services">
      <div className="container section__inner">
        <div className="services">
          <ul className="servicesList">
            {categories.map(item => (
              <ServiceCard key={item.id} item={item} />
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};
