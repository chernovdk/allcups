import React from 'react';
import './CardsSliderSection.scss';
import { CardCompetition } from '../components/CardCompetition';
import { CardTask } from '../components/CardTask';
import { CompetitionSlider, TaskSlider } from '../sliders';
import { Badge, SectionContainer, SectionHead } from 'ui/components';
import { useSelector } from 'react-redux';
import { contestsSelector, tasksSelector } from 'store';

type Props = {};

export const CardsSliderSection: React.FC<Props> = () => {
  const contests = useSelector(contestsSelector);
  const tasks = useSelector(tasksSelector);

  const pairedContests: Array<any[]> = !Array.isArray(contests)
    ? null
    : contests.slice(0, 5).reduce((acc, contest: any, index: number) => {
        if (index % 2 === 0) return [...acc, [contest]];
        acc[acc.length - 1].push(contest);
        return acc;
      }, []);

  const filteredTasks: Array<any> | null = !Array.isArray(tasks)
    ? null
    : tasks.slice(0, 10);

  return (
    <React.Fragment>
      {Array.isArray(pairedContests) && pairedContests.length && (
        <SectionContainer>
          <div className="iContent">
            <SectionHead
              right={() => (
                <a href="/" className="btn btn--link">
                  Смотреть все
                </a>
              )}
            >
              Активные соревнования
              <Badge count={contests.length} />
            </SectionHead>
            <CompetitionSlider>
              {pairedContests.map((pair, index) => (
                <React.Fragment key={index}>
                  {!!pair[0] && <CardCompetition contest={pair[0]} />}
                  {!!pair[1] && <CardCompetition contest={pair[1]} />}
                </React.Fragment>
              ))}
            </CompetitionSlider>
          </div>
        </SectionContainer>
      )}
      {Array.isArray(filteredTasks) && filteredTasks.length && (
        <SectionContainer>
          <div className="iContent">
            <SectionHead
              right={() => (
                <a href="/" className="btn btn--link">
                  Смотреть все
                </a>
              )}
            >
              Все задачи <Badge count={tasks.length} />
            </SectionHead>

            <TaskSlider>
              {filteredTasks.map(task => (
                <div className="swiper-slide" key={task.id}>
                  <CardTask task={task} />
                </div>
              ))}
            </TaskSlider>
          </div>
        </SectionContainer>
      )}
    </React.Fragment>
  );
};
