import React from 'react';
import './MainAside.scss';
import { SocialsList, ServicesListItem } from '../components';
import { useSelector } from 'react-redux';
import { categoriesSelector } from 'store';

type Props = {};

export const MainAside: React.FC<Props> = () => {
  const categories = useSelector(categoriesSelector);
  return (
    <aside className="aside">
      <div className="aside__inner">
        <div className="aside__body">
          {Array.isArray(categories) && (
            <ul className="servicesList servicesList--xs">
              {categories.map(category => (
                <ServicesListItem key={category.id} item={category} />
              ))}
            </ul>
          )}
        </div>

        <div className="aside__footer">
          <ul className="asideLinks">
            <li className="asideLinks__item">
              <a href="/">FAQ</a>
            </li>
          </ul>
          <SocialsList />
          <div className="copyright">© Cups.mail.ru — All Rights Reserved.</div>
        </div>
      </div>
    </aside>
  );
};
