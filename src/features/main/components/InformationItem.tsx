import React from 'react';
import './InformationItem.scss';
import { SvgIcon } from 'ui/components';

type Props = {};

export const InformationItem: React.FC<Props> = () => {
  return (
    <li className="informationList__item">
      <div className="card card--info">
        <div className="card__body">
          <div className="card__title">Получи бесценный опыт</div>
          <div className="card__subtitle">
            Улучшай свои навыки в решении различных олимпиадных
            <br />и боевых задач
          </div>
        </div>
        <div className="card__button">
          <a href="/" className="btn btn--move">
            <span>Подробнее</span>
            <SvgIcon icon="arrow_right" />
          </a>
        </div>
      </div>
    </li>
  );
};
