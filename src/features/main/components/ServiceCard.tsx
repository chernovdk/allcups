import React from 'react';
import { SvgIcon } from 'ui/components';
import './ServiceCard.scss';

type Props = {
  item: any;
};

export const ServiceCard: React.FC<Props> = ({ item }) => {
  const { icon, links, name, emblem, slug, shortDescription } = item;
  const decor = 'design_decor';

  return (
    <li className="servicesList__item">
      <div className="card card--services">
        <a href="/" className="card__link">
          {' '}
        </a>
        <div className="card__head">
          <div className="card__logo">
            {/* <SvgIcon icon={icon} /> */}
            <img src={emblem} alt={slug} />
          </div>
          <div className="card__title">{name}</div>
        </div>
        <div className="card__body">
          <div className="card__text">{shortDescription}</div>
        </div>
        <div className={`card__decor icon-${decor}`}>
          <SvgIcon icon={decor} />
        </div>
      </div>
    </li>
  );
};
