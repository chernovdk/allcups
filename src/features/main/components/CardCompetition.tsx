import React from 'react';
import './CardCompetition.scss';
import { SvgIcon } from 'ui/components';

type Props = {
  contest: any;
};

export const CardCompetition: React.FC<Props> = ({ contest }) => {
  const { id, name, category, registrationTill, image } = contest || {};
  return (
    <div className="card--competition">
      <a href="/" className="card__link">
        {' '}
      </a>
      <div
        className="card__bg"
        style={{ backgroundImage: `url('${image}')` }}
      ></div>
      <div className="card__inner">
        <div className="card__left">
          <div className="card__category">
            <div className="categoryTitle">
              <span
                className="decor"
                style={{ backgroundColor: '#EDBC2C' }}
              ></span>
              <span className="title" style={{ color: '#EDBC2C' }}>
                Искусственный интеллект
              </span>
            </div>
          </div>
          <div className="card__title">{name}</div>
          <div className="card__subtitle">
            Открытое соревнование по программированию искусственного интеллекта.
            Попробуйте свои силы в программировании игровой стратегии!
          </div>
        </div>
        <div className="card__right">
          <div className="card__timer">
            <div className="timer">
              <div className="timer__title">До конца осталось:</div>
              <div className="timer__body">
                <span>43 дня</span>
                <span className="delimiter">:</span> <span>17 часов</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="card__button">
        <a href="/" className="btn btn--move">
          <span>Подробнее</span>
          <SvgIcon icon="arrow_right" />
        </a>
      </div>
    </div>
  );
};
