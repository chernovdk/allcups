import React from 'react';
import './SocialsList.scss';

type Props = {};

export const SocialsList: React.FC<Props> = () => {
  return (
    <ul className="socialsList">
      <li className="socialsList__item">
        <a href="/" className="socialLink">
          <span className="socialLink__title socialLink__title--large">
            facebook
          </span>
          <span className="socialLink__title socialLink__title--small">fb</span>
        </a>
      </li>
      <li className="socialsList__item">
        <a href="/" className="socialLink">
          <span className="socialLink__title socialLink__title--large">vk</span>
          <span className="socialLink__title socialLink__title--small">vk</span>
        </a>
      </li>
      <li className="socialsList__item">
        <a href="/" className="socialLink">
          <span className="socialLink__title socialLink__title--large">
            Instagram
          </span>
          <span className="socialLink__title socialLink__title--small">
            Inst
          </span>
        </a>
      </li>
    </ul>
  );
};
