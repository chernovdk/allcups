import React, { useState } from 'react';
import { SvgIcon } from 'ui/components';
import classNames from 'classnames';

type Props = {
  item: any;
};

export const ServicesListItem: React.FC<Props> = ({ item }) => {
  const { icon, links, name, emblem, slug } = item;
  const [isActive, setIsActive] = useState(false);

  const toggleActive = () => setIsActive(isActive => !isActive);

  return (
    <li
      className={classNames('servicesList__item', {
        'is-active': isActive,
      })}
    >
      {!links || !links.length ? (
        <a href="/">
          <div className="servicesList__head">
            <div className="servicesList__icon">
              {/* <SvgIcon icon={icon} /> */}
              <img src={emblem} alt={slug} />
            </div>
            <div className="servicesList__title">{name}</div>
          </div>
        </a>
      ) : (
        <div
          className={classNames('accordion', {
            'is-active': isActive,
          })}
        >
          <div
            onClick={toggleActive}
            className="accordion__head servicesList__head"
          >
            <div className="servicesList__icon">
              <SvgIcon icon={icon} />
            </div>
            <div className="servicesList__title">{name}</div>

            <div className="accordion__arrow">
              <SvgIcon icon="chevron_bottom" />
            </div>
          </div>
          <div className="accordion__body servicesList__body">
            <ul className="servicesListLinks">
              {links.map((item: string) => (
                <li key={item}>
                  <a href="/">{item}</a>
                </li>
              ))}
            </ul>
          </div>
        </div>
      )}
      <span
        className="servicesList__borderRight"
        style={{ backgroundColor: '#CF62E6' }}
      ></span>
    </li>
  );
};
