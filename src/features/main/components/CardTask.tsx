import React, { Fragment } from 'react';
import { SvgIcon } from 'ui/components';
import './CardTask.scss';
import classNames from 'classnames';

type Props = {
  type?: 'primary' | 'secondary';
  task: any;
};

export const CardTask: React.FC<Props> = ({ type = 'primary', task }) => {
  const { name, categoryName, categoryColor } = task || {};
  return (
    <div
      className={classNames('card--task', {
        'card--taskSecondary': type === 'secondary',
      })}
    >
      {type === 'primary' && (
        <React.Fragment>
          <span className="card__arrow">
            <SvgIcon icon="chevron_right" />
          </span>
          <a href="/" className="card__link">
            {' '}
          </a>
        </React.Fragment>
      )}

      <div className="card__category">
        <div className="categoryTitle">
          <span
            className="decor"
            style={{ backgroundColor: categoryColor }}
          ></span>
          <span className="title" style={{ color: categoryColor }}>
            {categoryName}
          </span>
        </div>
      </div>
      <div className="card__title">{name}</div>
      {type === 'secondary' && (
        <React.Fragment>
          <div className="card__subtitle">12 июня 2019 - 24 августа 2019</div>
          <div className="card__bottom">
            <a href="/" className="btn btn--withIcon icon-file">
              <SvgIcon icon="file" />
              <span>Результаты</span>
            </a>

            <a href="/" className="btn btn--withIcon icon-winner">
              <SvgIcon icon="winner" />
              <span>Победитель</span>
            </a>
          </div>
        </React.Fragment>
      )}
    </div>
  );
};
