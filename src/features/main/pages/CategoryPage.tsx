import React, { Fragment } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { MainAside, CategoryBanner } from '../sections';
import { SectionContainer } from 'ui/components/section-container';
import { SectionHead, Badge } from 'ui/components';
import { CardCompetition, CardTask } from '../components';
import './CategoryPage.scss';

const fakeTask = {
  id: 710,
  name: 'Оценка производительности',
  categoryName: 'Категория',
  categoryColor: '#CCCCCC',
};

const fakeContest = {
  id: 3,
  name: 'Технокубок 2018',
  category: null,
  registrationTill: null,
  image: null,
};

type IProps = RouteComponentProps & {};

export const CategoryPage: React.FC<IProps> = () => {
  return (
    <Fragment>
      <MainAside />
      <div className="pageWrapper">
        <CategoryBanner />

        <SectionContainer>
          <SectionHead>
            Активные соревнования <Badge count={14} color="#73CA86" />
          </SectionHead>
          <div className="cardsList--competition">
            <CardCompetition contest={fakeContest} />
            <CardCompetition contest={fakeContest} />
            <CardCompetition contest={fakeContest} />
          </div>
        </SectionContainer>

        <SectionContainer>
          <SectionHead>
            Все задачи <Badge count={67} color="#73CA86" />
          </SectionHead>
          <div className="cardsList--task">
            <CardTask task={fakeTask} type="secondary" />
            <CardTask task={fakeTask} type="secondary" />
            <CardTask task={fakeTask} type="secondary" />
            <CardTask task={fakeTask} type="secondary" />
            <CardTask task={fakeTask} type="secondary" />
            <CardTask task={fakeTask} type="secondary" />
          </div>
        </SectionContainer>
      </div>
    </Fragment>
  );
};
