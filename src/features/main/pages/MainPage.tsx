import React, { Fragment } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import {
  MainAside,
  Banner,
  Services,
  CardsSliderSection,
  InformationSection,
} from '../sections';
import { useSelector } from 'react-redux';
import { categoriesSelector, contestsSelector, tasksSelector } from 'store';

type IProps = RouteComponentProps & {};

export const MainPage: React.FC<IProps> = () => {
  const categories = useSelector(categoriesSelector);
  const contests = useSelector(contestsSelector);
  const tasks = useSelector(tasksSelector);

  console.log({ categories, contests, tasks });
  return (
    <Fragment>
      <MainAside />
      <div className="pageWrapper">
        <Banner />
        <Services />
        <CardsSliderSection />
        <InformationSection />
      </div>
    </Fragment>
  );
};
