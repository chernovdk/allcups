import React, { useEffect } from 'react';
import './CompetitionSlider.scss';
import Swiper from 'swiper';
import { SliderControls } from 'ui/components';

type Props = {};

export const CompetitionSlider: React.FC<Props> = React.memo(({ children }) => {
  useEffect(() => {
    const competeSwiper = new Swiper('.competition-swiper-container', {
      watchOverflow: true,
      slidesPerView: 1,
      spaceBetween: 20,
      touchRatio: 1,
      navigation: {
        nextEl: '.competition-swiper-container + div .swiper-button-next',
        prevEl: '.competition-swiper-container + div .swiper-button-prev',
      },
      breakpoints: {
        900: {
          touchRatio: 0,
          spaceBetween: 10,
          slidesPerView: 1,
          slidesPerColumn: 1,
        },
      },
    });
    return () => {
      competeSwiper.destroy(true, true);
    };
  }, []);

  return (
    <div className="competitionSlider">
      <div
        className="competition-swiper-container swiper-container"
        data-swiper-slides="1"
      >
        <div className="swiper-wrapper">
          {React.Children.map(children, (child, index) => (
            <div className="swiper-slide" key={index}>
              {child}
            </div>
          ))}
        </div>
      </div>
      <SliderControls />
    </div>
  );
});
