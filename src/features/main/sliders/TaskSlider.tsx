import React, { useEffect } from 'react';
import Swiper from 'swiper';
import { SliderControls } from 'ui/components';
import './TaskSlider.scss';

type Props = {};

export const TaskSlider: React.FC<Props> = React.memo(({ children }) => {
  useEffect(() => {
    const taskSwiper = new Swiper('.task-swiper-container', {
      watchOverflow: true,
      slidesPerView: 1,
      spaceBetween: 20,
      touchRatio: 1,
      navigation: {
        nextEl: '.task-swiper-container + div .swiper-button-next',
        prevEl: '.task-swiper-container + div .swiper-button-prev',
      },
      breakpoints: {
        900: {
          touchRatio: 0,
          spaceBetween: 10,
          slidesPerView: 4,
          slidesPerColumn: 1,
        },
      },
    });
    return () => {
      taskSwiper.destroy(true, true);
    };
  }, []);

  return (
    <div className="taskSlider">
      <div
        className="task-swiper-container swiper-container"
        data-swiper-slides="4"
        data-mobile-columns="2"
        data-swiper-slides-mobile="1"
      >
        <div className="swiper-wrapper">
          {React.Children.map(children, (child, index) => (
            <div className="swiper-slide" key={index}>
              {child}
            </div>
          ))}
        </div>
      </div>
      <SliderControls />
    </div>
  );
});
