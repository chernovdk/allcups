import React, { useEffect } from 'react';
import './InformationSlider.scss';
import Swiper from 'swiper';

type Props = {};

export const InformationSlider: React.FC<Props> = React.memo(({ children }) => {
  useEffect(() => {
    const informationSlider = new Swiper(
      '.informationSlider .swiper-container',
      {
        watchOverflow: true,
        slidesPerView: 1,
        spaceBetween: 10,
        pagination: {
          el: '.informationSlider .swiper-pagination',
          clickable: true,
        },
      }
    );
    return () => {
      informationSlider.destroy(true, true);
    };
  }, []);

  return (
    <div className="informationSlider">
      <div className="swiper-container">
        <div className="swiper-wrapper">
          {React.Children.map(children, (child, index) => (
            <div className="swiper-slide" key={index}>
              {child}
            </div>
          ))}
        </div>
        <div className="swiper-pagination"></div>
      </div>
    </div>
  );
});
