import axios, { AxiosResponse } from 'axios';
import { camelizeKeys, decamelizeKeys } from 'humps';
import { stringify } from 'qs';
import { getAuth } from 'services/auth';

const BACKEND_API = process.env.REACT_APP_BACKEND_API;

const httpClient = axios.create({
  baseURL: BACKEND_API,
  responseType: 'json',
  paramsSerializer: params => {
    return stringify(decamelizeKeys(params));
  },
});

httpClient.interceptors.request.use(request => {
  const tokens = getAuth();
  if (tokens.accessToken) {
    request.headers['authorization'] = `Bearer ${tokens.accessToken}`;
  }
  return request;
});

httpClient.interceptors.request.use(request => {
  request.data =
    request.data && !(request.data instanceof FormData)
      ? decamelizeKeys(request.data)
      : request.data;
  return request;
});

httpClient.interceptors.response.use(response => {
  return camelizeKeys(response) as AxiosResponse;
});

export { httpClient };
