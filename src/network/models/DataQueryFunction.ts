export interface DataQueryFunction {
  (
    variables?: {
      [key: string]: any;
    } | null,
    options?: { [key: string]: any }
  ): Promise<any>;
}
