export interface DataMutationFunction {
  (
    variables?: {
      body?: { [key: string]: any };
      [key: string]: any;
    },
    options?: { [key: string]: any }
  ): Promise<any>;
}
