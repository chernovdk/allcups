import { DataMutationFunction } from 'network/models';
import { httpClient } from 'network/index';

export const uploadImageMutation: DataMutationFunction = (
  variables = {}
): Promise<any> => {
  const { body: imageBin } = variables;
  let formData = new FormData();

  if (imageBin) formData.append('image', imageBin as File);

  return httpClient.post(`images`, formData);
};
