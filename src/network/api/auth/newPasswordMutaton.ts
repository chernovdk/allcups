import { DataMutationFunction } from 'network/models';
import { httpClient } from 'network';

export const newPasswordMutaton: DataMutationFunction = (
  variables = {}
): Promise<any> => {
  const { body } = variables;
  return httpClient.put('/auth/new_password', body);
};
