import { DataMutationFunction } from 'network/models';
import { httpClient } from 'network';

export const loginMutaton: DataMutationFunction = (
  variables = {}
): Promise<any> => {
  const { body } = variables;
  return httpClient.post('/auth', body);
};
