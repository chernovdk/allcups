import { DataMutationFunction } from 'network/models';
import { httpClient } from 'network';

export const restoreMutaton: DataMutationFunction = (
  variables = {}
): Promise<any> => {
  const { body } = variables;
  return httpClient.post('/auth/password_restore', body);
};
