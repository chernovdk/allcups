import { DataQueryFunction } from 'network/models';
import { httpClient } from 'network';

export const queryTasks: DataQueryFunction = (): Promise<any> => {
  return httpClient.get(`/tasks/`);
};
