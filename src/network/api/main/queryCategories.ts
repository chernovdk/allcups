import { DataQueryFunction } from 'network/models';
import { httpClient } from 'network';

export const queryCategories: DataQueryFunction = (): Promise<any> => {
  return httpClient.get(`/categories/`);
};
