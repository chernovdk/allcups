import { DataQueryFunction } from 'network/models';
import { httpClient } from 'network';

export const queryContests: DataQueryFunction = (): Promise<any> => {
  return httpClient.get(`/contests/`);
};
