export interface PackageRoutesConfig {
  layout: string;
  routes: Array<{
    path: string;
    component?: React.FC<any> | null;
    exact?: Boolean;
    redirect?: string;
    layout?: string;
  }>;
}
