export interface RouteConfig {
  path: string;
  component: React.FC<any> | null;
  layout: string;
  exact: boolean;
  redirect: string | null;
}
