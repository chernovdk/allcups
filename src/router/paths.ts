export const PATH = {
  // MAIN
  MAIN: '/',
  CATEGORY: '/category/:name',
  LOGIN: '/login',
  SIGNUP: '/signup',
};
