import React from 'react';
import {
  Redirect,
  matchPath,
  withRouter,
  RouteComponentProps,
} from 'react-router-dom';
import { RouteConfig } from './models';
import { layoutMap } from 'common/layouts';

type IProps = RouteComponentProps & {
  routes: Array<RouteConfig>;
};

export const SwitchRoutes = withRouter(
  ({ routes, location, history }: IProps) => {
    let match;
    const matchedRoute = routes.find(route => {
      match = matchPath(location.pathname, {
        path: route.path,
        exact: route.exact,
      });
      return !!match;
    });

    if (!matchedRoute || !match) return null;

    const { layout, redirect, component: Component } = matchedRoute;

    const LayoutComponent = layoutMap[layout].component;

    const routerProps = {
      history,
      location,
      match,
    };

    return (
      <LayoutComponent {...layoutMap[layout].props} {...routerProps}>
        {redirect ? (
          <Redirect to={redirect} />
        ) : Component ? (
          <Component {...routerProps} />
        ) : null}
      </LayoutComponent>
    );
  }
);
