import { PATH } from '../paths';
import { PackageRoutesConfig } from 'router/models';
import { MainPage, CategoryPage } from 'features/main/pages';
import { LAYOUT } from 'common/layouts';

export const guestRoutesConfig: PackageRoutesConfig = {
  layout: LAYOUT.MAIN,
  routes: [
    {
      path: PATH.MAIN,
      component: MainPage,
      exact: true,
    },
    {
      path: PATH.CATEGORY,
      component: CategoryPage,
    },
    {
      path: '*',
      redirect: PATH.MAIN,
    },
  ],
};
