import React from 'react';
import { Router } from 'react-router-dom';
import { history } from 'router/history';
import { Provider as StoreProvider } from 'react-redux';
import { SwitchRoutes, generateRoutes, guestRoutesConfig } from 'router';
import { store, fetchMainData } from 'store';

const guestRoutes = generateRoutes([guestRoutesConfig]);

store.dispatch(fetchMainData());

const App: React.FC = () => {
  return (
    <StoreProvider store={store}>
      <Router history={history}>
        <SwitchRoutes routes={guestRoutes} />
      </Router>
    </StoreProvider>
  );
};

export default App;
