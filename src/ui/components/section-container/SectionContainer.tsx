import React, { FC } from 'react';
import './SectionContainer.scss';

type Props = {};

export const SectionContainer: FC<Props> = ({ children }) => {
  return (
    <div className="section section--container">
      <div className="container section__inner">{children}</div>
    </div>
  );
};
