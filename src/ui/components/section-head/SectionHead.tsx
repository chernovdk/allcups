import React, { FC } from 'react';
import './SectionHead.scss';

type Props = {
  right?(): React.ReactNode;
  left?(): React.ReactNode;
};

export const SectionHead: FC<Props> = ({ children, right, left }) => {
  return (
    <div className="sectionHead">
      <div className="sectionHead__left">
        <div className="section__title">
          {typeof left === 'function' ? left() : children}
        </div>
      </div>
      {typeof right === 'function' && (
        <div className="sectionHead__right">{right()}</div>
      )}
    </div>
  );
};
