import React, { FC } from 'react';
import { Link } from 'react-router-dom';

type Props = {};

export const Button: FC<Props> = ({ children }) => {
  return (
    <a href="/signup" className="btn btn--border">
      {children}
    </a>
  );
};
