import React, { FC } from 'react';
import { SvgIcon } from 'ui/components/svg-icon';
import './SliderControls.scss';

type Props = {};

export const SliderControls: FC<Props> = ({ children }) => {
  return (
    <div className="swiper-controls">
      <div className="swiper-button swiper-button-next">
        <SvgIcon icon="chevron_right" />
      </div>
      <div className="swiper-button swiper-button-prev">
        <SvgIcon icon="chevron_left" />
      </div>
    </div>
  );
};
