import { ReactComponent as VisokonagruzServices } from 'assets/icons/visokonagruz_services.svg';
import { ReactComponent as DesignInterface } from 'assets/icons/design_interface.svg';
import { ReactComponent as IskustIntelekt } from 'assets/icons/iskust_intelekt.svg';
import { ReactComponent as MashinObuch } from 'assets/icons/mashin_obuch.svg';
import { ReactComponent as SportProgram } from 'assets/icons/sport_program.svg';
import { ReactComponent as ChevronBottom } from 'assets/icons/chevron_bottom.svg';
import { ReactComponent as ChevronRight } from 'assets/icons/chevron_right.svg';
import { ReactComponent as ChevronLeft } from 'assets/icons/chevron_left.svg';
import { ReactComponent as BannerPicture } from 'assets/icons/banner_picture.svg';
import { ReactComponent as ArrowRight } from 'assets/icons/arrow_right.svg';
import { ReactComponent as VsDecor } from 'assets/icons/vs_decor.svg';
import { ReactComponent as MashinDecor } from 'assets/icons/mashin_decor.svg';
import { ReactComponent as DesignDecor } from 'assets/icons/design_decor.svg';
import { ReactComponent as IiDecor } from 'assets/icons/ii_decor.svg';
import { ReactComponent as SportDecor } from 'assets/icons/sport_decor.svg';
import { ReactComponent as File } from 'assets/icons/file.svg';
import { ReactComponent as Winner } from 'assets/icons/winner.svg';

export const svgIconMap: { [key: string]: React.FC } = {
  visokonagruz_services: VisokonagruzServices,
  chevron_bottom: ChevronBottom,
  chevron_right: ChevronRight,
  chevron_left: ChevronLeft,
  design_interface: DesignInterface,
  iskust_intelekt: IskustIntelekt,
  mashin_obuch: MashinObuch,
  sport_program: SportProgram,
  banner_picture: BannerPicture,
  arrow_right: ArrowRight,
  vs_decor: VsDecor,
  mashin_decor: MashinDecor,
  design_decor: DesignDecor,
  ii_decor: IiDecor,
  sport_decor: SportDecor,
  file: File,
  winner: Winner,
};
