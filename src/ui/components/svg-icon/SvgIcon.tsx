import React from 'react';
import { svgIconMap } from './svgIconMap';

type Props = {
  icon: string;
};

export const SvgIcon: React.FC<Props> = ({ icon }) => {
  const IconComp = svgIconMap[icon];
  return IconComp ? <IconComp /> : null;
};
