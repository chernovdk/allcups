import React, { FC } from 'react';

type Props = {
  count: number;
  color?: string;
};

export const Badge: FC<Props> = ({ count, color }) => {
  return (
    <span
      className="badge badge--circle"
      style={color ? { backgroundColor: color } : {}}
    >
      {count || 0}
    </span>
  );
};
