import { createAction } from 'redux-starter-kit';

//MAIN
export const setCategories = createAction<any>('SET_CATEGORIES');
export const setContests = createAction<any>('SET_CONTESTS');
export const setTasks = createAction<any>('SET_TASKS');
