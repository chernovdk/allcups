import { createReducer } from 'redux-starter-kit';
import { setCategories, setContests, setTasks } from './actions';

export const mainReducer = createReducer<any>(
  { categories: null, tasks: null, contests: null },
  {
    [setCategories.type]: (state, action) => ({
      ...state,
      categories: action.payload,
    }),
    [setContests.type]: (state, action) => ({
      ...state,
      contests: action.payload,
    }),
    [setTasks.type]: (state, action) => ({
      ...state,
      tasks: action.payload,
    }),
  }
);
