import { ThunkDispatch } from 'redux-thunk';
import { queryCategories, queryContests, queryTasks } from 'network';
import { setCategories, setContests, setTasks } from './actions';

export const fetchCategories = () => (dispatch: ThunkDispatch<any, any, any>) =>
  queryCategories().then(res => dispatch(setCategories(res.data)));

export const fetchContests = () => (dispatch: ThunkDispatch<any, any, any>) =>
  queryContests().then(res => dispatch(setContests(res.data)));

export const fetchTasks = () => (dispatch: ThunkDispatch<any, any, any>) =>
  queryTasks().then(res => dispatch(setTasks(res.data)));

export const fetchMainData = () => (dispatch: ThunkDispatch<any, any, any>) => {
  dispatch(fetchCategories());
  dispatch(fetchContests());
  dispatch(fetchTasks());
};
