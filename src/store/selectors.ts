import { createSelector } from 'redux-starter-kit';

//MAIN
export const mainSelector = (state: any) => state.main;
export const categoriesSelector = createSelector<any, any, Array<any>>(
  mainSelector,
  state => state.categories
);
export const contestsSelector = createSelector<any, any, Array<any>>(
  mainSelector,
  state => state.contests
);
export const tasksSelector = createSelector<any, any, Array<any>>(
  mainSelector,
  state => state.tasks
);
