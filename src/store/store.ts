import { configureStore } from 'redux-starter-kit';
import { mainReducer } from './reducers';

const store = configureStore({
  reducer: { main: mainReducer },
});

// if (process.env.NODE_ENV !== 'production' && module.hot) {
//   module.hot.accept('./reducers', () => store.replaceReducer(rootReducer))
// }

export { store };
